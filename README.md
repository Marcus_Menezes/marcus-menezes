IoasysAPP 

Para a utilização basta efetuar o login a partir do email e senha padrões, na home temos a lista de empresas ao clicar em uma das empresas temos o detalhe da mesma. Caso você queira filtrar por nome deve clicar no icone de filtro, ao clicar selecionar a opção "Enterprise Name" que irá abrir um input para entrar com o nome da empresa, após basta apenas clicar no icone de lupa que irá filtrar pelo nome da empresa. Caso precise filtrar pelo tipo basta clicar no mesmo icone de filtro e selecionar "Type" onde abrirá uma lista de tipos para selecionar o tipo e será filtrado.

Efetuei todas as telas com os seus seguintes endpoints, meu proximo passo seria a construção do fluxo dentro do redux / redux-saga onde tenho conhecimento mas por algum erro interno / bug não consigo utilizar o debug nativo para poder utilizar o React Native Debbuger, efetuei a construção no "escuro" de um dos fluxo do redux para desmostrar como ficaria. Por falta de tempo não tive como arrumar esse bug mas mesmo assim demostrei como ficaria dentro da store no fluxo de "getEnterprises".

Alguns components e utils foram utilizados de um outro peojeto pessoal meu, caso haja alguma duvida referente aos components / utils.

Libs Utilizadas

react-native-community/async-storage=> utilizado para armazenar informações do usuario de login localmente
react-navigation/drawer => utilizado para criação da navegação do drawer lateral
react-navigation/native => utilizado para a criação da navegação
react-navigation/stack => utilizado para a criação da navegação
lottie-react-native => utilizado para a criação do loading
react => build da aplicação
react-native => build da aplicação
react-native-animatable => utilizado para criar animações pelo app
react-native-modalize => utilizado para a criação do component default de modal
react-native-paper => utilizado para adição do drawer na aplicação
react-native-reanimated => utilizado para animações mais especificas na tela HomeScreen
react-native-screens => utilizado pelo drawer do paper
react-native-safe-area-context => utilizado pelo drawer do paper
react-native-gesture-handler => utilizado pelo drawer do paper
react-native-splash-screen => utilizado para splash screen na inicialização do app
react-native-vector-icons => utilizado para adição de icones pelo app
react-redux => utilizado para implementação do redux pelo app
redux => utilizado para implementação do redux pelo app
redux-devtools-extension => utilizado para debugar o app
redux-saga => utilizado para implementação do saga pelo app