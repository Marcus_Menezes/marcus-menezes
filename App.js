import AsyncStorage from '@react-native-community/async-storage';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import React, { useEffect } from 'react';
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  Easing, 
  StatusBar, 
  View,
  Image,
} from 'react-native';
import { Provider as PaperProvider } from 'react-native-paper';
import SplashScreenElement from 'react-native-splash-screen';
import { Provider } from 'react-redux';
import { COLORS } from './src/constants/colors';
import { AuthContext } from './src/context/context';
import store from './src/redux/index'
import SignInScreen from './src/pages/auth/LoginScreen/SignInScreen';
import { DrawerContent } from './src/pages/drawer/DrawerContent';
import MainTabScreen from './src/pages/routes/MainTabScreen';
import RootStackScreen from './src/pages/routes/RootStackScreen';
import { GetUser, SetUser } from './src/utils/AsyncUtil';

const IMAGE_INTRO = './src/assets/logo_ioasys.png'

const { height, width } = Dimensions.get('window')

const Drawer = createDrawerNavigator();

const Stack = createStackNavigator();

const App = () => {

  function CreateNavigator() {

    return (
      <Drawer.Navigator drawerType={'slide'} screenOptions={{ headerShown: false, ...TransitionPresets.SlideFromRightIOS }} statusBarAnimation={"slide"} drawerContent={props => <DrawerContent {...props} />}>
        <Drawer.Screen name="HomeDrawer" component={MainTabScreen} />
        <Drawer.Screen name="SignInScreen" component={SignInScreen} />
      </Drawer.Navigator>
    )
  }

  const loginReducer = (prevState, action) => {
    switch (action.type) {
      case 'RETRIEVE_TOKEN':
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGIN':
        return {
          ...prevState,
          uid: action.uid,
          client: action.client,
          userToken: action.token,
          isLoading: false,
          isSignout: false,
        };
      case 'LOGOUT':
        return {
          ...prevState,
          uid: null,
          client: null,
          userToken: undefined,
          isLoading: false,
          isSignout: true,
        };
    }
  };

  const initialLoginState = {
    isLoading: true,
    uid: null,
    userToken: null,
  };

  const [loginState, dispatch] = React.useReducer(loginReducer, initialLoginState);
  const [progress, setprogress] = React.useState(new Animated.Value(0))
  const [ref, setRef] = React.useState(null)
  
  const authContext = React.useMemo(() =>
    ({
      signIn: async (foundUser) => {
        const { userToken, uid, client } = foundUser;

        try {
          await SetUser(foundUser) 
        }
        catch (e) {
          console.log(e);
        }

        dispatch({ type: 'LOGIN', uid: uid, token: userToken, client: client });
      },
      signOut: async () => {
        try {
          await AsyncStorage.clear();
        }
        catch (e) {
          console.log(e);
        }
        dispatch({ type: 'LOGOUT' });
      },
      signUp: () => {

      },
      toggleTheme: () => {
        setIsDarkTheme(isDarkTheme => !isDarkTheme);
      }
    }), []);

  useEffect(() => {
    setTimeout(async () => {
      let userToken;
      userToken = null;
      try {
        const User = await GetUser()
        userToken = User?.userToken
      }
      catch (e) {
        console.log(e);
      }
      dispatch({ type: 'RETRIEVE_TOKEN', token: userToken });
    }, 1);
  }, []);

  if (loginState.isLoading || progress != 1) {
    Animated.timing(progress, { toValue: 1, duration: 4500, useNativeDriver: true, easing: Easing.linear, }).start(() => {
      setTimeout(() => setprogress(1), 5000)
    });
    try { SplashScreenElement.hide(); } catch (e) { }
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: COLORS.BRANCO_APP }}>
        <StatusBar backgroundColor={COLORS.BRANCO_APP} barStyle="light-content" />
        <Image
          source={require(IMAGE_INTRO)}
          style={{ height: height / 1.5, width: width * 0.8, resizeMode: 'contain' }}
        />
      </View>
    );
  }

  if (progress === 1) {

    return (
      // <Provider store={store}>
        <PaperProvider>

          <AuthContext.Provider value={authContext}>
            <NavigationContainer>
              <Stack.Navigator>
                {
                  loginState.userToken !== undefined
                    ?
                    <Stack.Screen
                      name="Home"
                      options={{
                        headerShown: false
                      }}
                      component={CreateNavigator} />
                    :

                    <Stack.Screen
                      name="Login"
                      options={{
                        headerShown: false
                        , animationTypeForReplace: loginState.isSignout ? 'pop' : 'push'
                        , animationEnabled: false
                      }}
                      component={RootStackScreen} />
                }
              </Stack.Navigator>
            </NavigationContainer>
          </AuthContext.Provider>

        </PaperProvider>
      // </Provider>
    );
  }
  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#FFF' }}>
      <ActivityIndicator color={COLORS.PRETO_APP}></ActivityIndicator>
    </View>
  );
}

export default App;

