import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Drawer } from 'react-native-paper';
import Animated from 'react-native-reanimated';
import { default as Icon, default as MaterialCommunityIcons } from 'react-native-vector-icons/MaterialCommunityIcons';
import { FONTS } from '../../../src/constants/fonts';
import { COLORS } from '../../constants/colors';
import { AuthContext } from '../../context/context';
import * as Animatable from 'react-native-animatable';

const TEXT_ALERT = 'This feature is still in development, please try again later!'

export function DrawerContent({ progress, ...props }) {

    const [isErrorButton, setIsErrorButton] = useState(false)

    const translateX = Animated.interpolateNode(progress, {
        inputRange: [0, 1],
        outputRange: [-80, 0],
    });

    const _Opacity = Animated.interpolateNode(progress, {
        inputRange: [0, 1],
        outputRange: [0, 1],
    })

    const { signOut } = React.useContext(AuthContext);

    function ErrorPages() {
        setIsErrorButton(true)
        setTimeout(() => {
            setIsErrorButton(false)
        }, 2000);
    }

    async function handleSignOut() {
        signOut()
    }
 
    return (
        <>
        <View style={{ flex: 1 }}>

            <DrawerContentScrollView
                showsVerticalScrollIndicator={false} {...props}>

                <Animated.View style={[styles.drawerContent, { opacity: _Opacity, transform: [{ translateX }] }]}>


                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialCommunityIcons
                                    name="home"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Home"
                            labelStyle={{ fontFamily: FONTS.Montserrat_Medium, fontSize: 11 }}
                            onPress={() => props.navigation.navigate('HomeDrawer')}
                        />
                       <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialCommunityIcons
                                    name="star"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Salvos"
                            labelStyle={{ fontFamily: FONTS.Montserrat_Medium, fontSize: 11 }}
                            onPress={() => ErrorPages()}
                        />

                        <DrawerItem

                            icon={({ color, size }) => (
                                <MaterialCommunityIcons
                                    name="cog-outline"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Settings"
                            labelStyle={{ fontFamily: FONTS.Montserrat_Medium, fontSize: 11 }}
                            onPress={() => ErrorPages()}
                        />


                        <DrawerItem
                            icon={({ color, size }) => (
                                <MaterialCommunityIcons
                                    name="page-layout-footer"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Termos de uso"
                            labelStyle={{ fontFamily: FONTS.Montserrat_Medium, fontSize: 11 }}
                            onPress={() => ErrorPages()}
                        />
                    </Drawer.Section>

                    {
                        isErrorButton && 
                        <Animatable.Text animation={'slideInLeft'} useNativeDriver style={{textAlign: 'center', marginHorizontal: '5%', color: COLORS.VERMELHO_APP}}>
                            {TEXT_ALERT}
                        </Animatable.Text>
                    }

                </Animated.View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem
                    icon={({ color, size }) => (
                        <Icon
                            name="exit-to-app"
                            color={color}
                            size={size}
                        />
                    )}
                    label="Sair"
                    labelStyle={{ fontFamily: FONTS.Montserrat_Medium }}
                    onPress={() => { handleSignOut() }}
                />
            </Drawer.Section>
        </View>
    </>
    );
}

const styles = StyleSheet.create({
    drawerContent:
    {
        flex: 1,
    },
    userInfoSection:
    {
        paddingLeft: 20,
    },
    title:
    {
        fontSize: 16,
        marginTop: 3,
        fontFamily: FONTS.Montserrat_Bold
    },
    caption:
    {
        fontSize: 14,
        lineHeight: 14,
        fontFamily: FONTS.Montserrat_Medium,
    },
    row:
    {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section:
    {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph:
    {
        fontFamily: FONTS.Montserrat_Medium,
        marginRight: 3,
    },
    drawerSection:
    {
        marginTop: 15,
    }
    , bottomDrawerSection:
    {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    }
    , preference:
    {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,

    },
});