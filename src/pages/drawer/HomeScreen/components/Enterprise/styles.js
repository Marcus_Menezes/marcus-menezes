import {StyleSheet,Dimensions} from 'react-native';
import { COLORS } from '../../../../../constants/colors';
import { FONTS } from '../../../../../constants/fonts';
const { height, width } = Dimensions.get('window') //Dimensões.

export default styles = StyleSheet.create({
    ItemContainer:
    {
      backgroundColor: COLORS.CINZAINPUT_APP,
      height: height * 0.1,
      maxHeight: height * 0.1,
      width: width * 0.9,
      maxWidth: width * 0.9,
      justifyContent: 'center',
      borderRadius: 15
    }
    , ItemImageContainer:
    {
      flex: 1,
      marginLeft: '5%'
    }
    , ItemImage:
    {
      width: 100
      , height: 100
      , borderRadius: 100
      , borderColor: COLORS.BRANCO_APP
    }
    , ItemTextContainer:
    {
      flex: 2,
      justifyContent: 'center',
      alignItems: 'center',
    }
    , ItemText:
    {
      fontFamily: FONTS.Montserrat_Black
      , fontSize: 10
      , flex: 1
      , maxWidth: 150
    }
    , ItemSubcategoryText:
    {
      fontSize: 15
      , color: COLORS.PRETO_APP
      , fontFamily: FONTS.Montserrat_Bold
    }
    , ItemTextName:
    {
      fontSize: 20
      , color: COLORS.BRANCO_APP
      , fontFamily: FONTS.Montserrat_Bold
    }
  })