import React from 'react';
import {
  Image,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import styles from './styles';
import { VectorIcon } from '../../../../../utils/VectorIconsUtil';

const ITEM_ANIMATION = 'slideInUp'
const URL_BASE_PHOTO = 'https://empresas.ioasys.com.br'

const Enterprise = ({ enterprise, navigateToInfo }) => {

  return (
    <Animatable.View animation={ITEM_ANIMATION} useNativeDriver style={styles.ItemContainer}>
      <TouchableOpacity
        onPress={() => navigateToInfo()}
      >
          <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>

            <View style={styles.ItemImageContainer}>
            {
              enterprise.photo ? <Image style={styles.ItemImage} resizeMode={'contain'} source={{ uri: URL_BASE_PHOTO + enterprise.photo }} /> :
              <VectorIcon type={2} IconSize={50} IconName={'image-off'} IconStyle={{justifyContent: 'center', marginLeft: '20%'}} />
            }  
            </View>
            
          
            <View style={styles.ItemTextContainer}>
              <Text numberOfLines={1} style={styles.ItemTextName}>{enterprise.enterprise_name ?? 'Enterprise'}</Text>
              <Text numberOfLines={1} style={styles.ItemSubcategoryText}>{enterprise.city} {(enterprise.city && enterprise.country) && '-'} {enterprise.country}</Text>
            </View>

          </View>
      </TouchableOpacity>
    </Animatable.View>
  )
}

export default Enterprise