import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Image,
    RefreshControl
} from 'react-native';
import Animated from 'react-native-reanimated';
import * as Animatable from 'react-native-animatable';
import { FocusAwareStatusBar } from '../../../components/StatusBar/StatusBar';
import { COLORS } from '../../../constants/colors';
import styles from './styles';
import { VectorIcon } from '../../../utils/VectorIconsUtil';
import { FlatList } from 'react-native';
import Enterprise from './components/Enterprise/index';
import { ModalizeComponent } from '../../../components/ModalizeComponent';
import { getEnterpriseByType, getListEnterprises, getEnterpriseByName } from '../../../services/EnterprisesServices';
import { distinctFunc } from '../../../utils/ArrayUtil';
import SnackBar from '../../../components/Snackbar/SnackBar';

const STATUS_BAR_COLOR = COLORS.BRANCO_APP

const IMAGE_INTRO = '../../../assets/logo_ioasys.png'
const TEXT_HEADER = 'Enterprises'

const MODAL_TITLE_BUTTON_TEXT = 'Choose the filter';
const MODAL_LEFT_BUTTON_TEXT = 'Enterprise Name';
const MODAL_RIGHT_BUTTON_TEXT = 'Type';
    
const HomeScreen = ({ navigation }) => {

    const scroll_x = React.useRef(new Animated.Value(0)).current;

    const _Fade = scroll_x.interpolate({ inputRange: [0, 100], outputRange: [1, 0], extrapolate: 'clamp' })

    const _Scale = scroll_x.interpolate({ inputRange: [0, 100], outputRange: [1, 0.5], extrapolate: 'clamp' })

    const [refreshing, setRefreshing] = useState(false)
    const [filterName, setFilterName] = useState(null)
    const [showFilter, setShowFilter] = useState(false)
    const [enterprises, setEnterprises] = useState([])
    const [enterprisesTypes, setEnterprisesTypes] = useState([])
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [propsFlatListModal, setPropsFlatListModal] = useState({})
    const [typeFooterModal, setTypeFooterModal] = useState(2)
    const [showCloseFilterType, setShowCloseFilterType] = useState(false)

    const [isErrorButton, setIsErrorButton] = useState(false)
    const [errorTextButton, setErrorTextButton] = useState(false)

    useEffect(() => {
        Mount()
    }, [])

    async function ListEnterprises() {
        const response = await getListEnterprises()

        if (response.status === 200) {
            setRefreshing(false)
            setEnterprises(response.enterprises)
            const allTypes = response.enterprises.map(item => item.enterprise_type)
            const allTypesFiltered = distinctFunc(allTypes, ["id"])
            setEnterprisesTypes(allTypesFiltered)
        } else {
            setRefreshing(false)
            setIsErrorButton(true)
            setErrorTextButton(response.error + ', try again later!' ?? 'Error when collecting Enterprises!')
        }
    }

    async function ListEnterpriseByName() {
        const response = await getEnterpriseByName(filterName)
        if (response.status === 200) {
            setRefreshing(false)
            setEnterprises(response.enterprises)
        } else {
            setRefreshing(false)
            setIsErrorButton(true)
            setErrorTextButton(response.error + ', try again later!' ??'Error when collecting Enterprises by name!')
        }
    }

    async function ListEnterpriseByType(type) {
        const response = await getEnterpriseByType(type)
        if (response.status === 200) {
            setRefreshing(false)
            setEnterprises(response.enterprises)
        } else {
            setRefreshing(false)
            setIsErrorButton(true)
            setErrorTextButton(response.error + ', try again later!' ?? 'Error when collecting Enterprises by type!')
        }
    }

    function Mount() {
        setRefreshing(true)
        ListEnterprises()
    }

    function onRefresh() {
        setRefreshing(true)
        Mount()
    }

    function onCloseFilter() {
        setShowFilter(false)
        setFilterName(false)
        Mount()
    }

    function onCloseModal() {
        setIsModalVisible(false)
        setTypeFooterModal(2)
        setPropsFlatListModal({})
    }

    function HandlePrimaryButton() {
        setShowFilter(true)
        onCloseModal()
    }

    function onPressRightIcon() {
        if (showFilter) 
            searchByName()
        else setIsModalVisible(true)
    }

    function searchByName() {
        setRefreshing(true)
        ListEnterpriseByName()
    }
    
    function searchByType(type) {
        setShowCloseFilterType(true)
        onCloseModal()
        setRefreshing(true)
        ListEnterpriseByType(type)
    }

    function onCloseFilterType() {
        setShowCloseFilterType(false)
        Mount()
    }

    function HandleSecondaryButton() {
        setTypeFooterModal(0)
        setPropsFlatListModal({
            showsVerticalScrollIndicator: false,
            scrollEventThrottle: 16,
            data: enterprisesTypes,
            renderItem: renderItemEnterpriseType,
            keyExtractor: item => item.id,
            contentContainerStyle: { paddingVertical: '10%' },
        })
    }

    const renderEnterprises = ({ item }) => 
        <Enterprise enterprise={item} navigateToInfo={() => { navigation.navigate('EnterpriseInfo', {enterpriseId: item.id}) }}/>

    const renderHeaderList = () => 
        <Text numberOfLines={1} style={styles.HeaderList}>{TEXT_HEADER}</Text>

    const renderItemEnterpriseType = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => searchByType(item.id)}>
                <Animatable.View animation={'slideInLeft'} useNativeDriver style={styles.EnterpriseTypeItem}>
                    <Text numberOfLines={1} style={styles.EnterpriseTypeItemText}>{item.enterprise_type_name}</Text>
                </Animatable.View>
            </TouchableOpacity>
        )
    }

    const renderFilter = () =>
        <TextInput
            placeholder="Enter your enterprise name"
            style={styles.textInput}
            autoCapitalize="words"
            value={filterName}
            textAlign="center"
            selectionColor={COLORS.PRETO_APP}
            placeholderTextColor={COLORS.BRANCO_APP}
            onChangeText={(val) => setFilterName(val)}
        />
        

    const renderRefreshControl = () =>
        <RefreshControl
            tintColor="transparent"
            colors={['transparent']}
            style={{ backgroundColor: 'transparent' }}
            refreshing={refreshing}
            onRefresh={onRefresh}
        />

    return(
        <>
            <Animated.ScrollView
                style={styles.container}
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                onScroll={Animated.event(
                [{ nativeEvent: { contentOffset: { y: scroll_x } } }],
                { useNativeDriver: true }
                )}
                refreshControl={renderRefreshControl()}
            >

            <FocusAwareStatusBar
            backgroundColor={STATUS_BAR_COLOR}
            barStyle="dark-content"
            />

            <Animatable.View animation="fadeInUp" duration={1000} useNativeDriver style={[styles.YourLocationContainer, { opacity: _Fade, transform: [{ scale: _Scale }] }]}>

                <Animatable.View useNativeDriver animation={'slideInLeft'} style={styles.TransparentYourLocationView}>
                {
                    showFilter ? 
                        renderFilter()
                    :
                    <Animated.View style={[styles.YourLocationSecondView, { marginleft: '4%', transform: [{ scale: _Scale }] }]}>
                        <Image source={require(IMAGE_INTRO)} style={styles.IjobLogoStyle} />
                    </Animated.View>
                }
                </Animatable.View>
                    {
                        showFilter ? 
                        <View style={styles.iconSearch}>
                            <TouchableOpacity onPress={() => onPressRightIcon()}>
                                <VectorIcon type={1} IconName="search" IconSize={26} IconColor={COLORS.PRETO_APP}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => onCloseFilter()}>
                                <VectorIcon type={2} IconName="close-circle-outline" IconSize={26} IconColor={COLORS.PRETO_APP}/>
                            </TouchableOpacity>
                        </View>
                        : 
                        <TouchableOpacity onPress={() => { showCloseFilterType ? onCloseFilterType() : onPressRightIcon()}}>
                            <Animatable.View useNativeDriver animation={'slideInRight'} style={styles.TransparentYourLocationView}>
                                <Animated.View style={[styles.YourLocationSecondView, { marginleft: '4%', transform: [{ scale: _Scale }] }]}>
                                    <VectorIcon type={2} IconName={showCloseFilterType ? "close-circle-outline" : "filter-plus"} IconSize={30} IconColor={COLORS.PRETO_APP}/>
                                </Animated.View>
                            </Animatable.View>
                        </TouchableOpacity>
                        
                    }

            </Animatable.View>

            <FlatList
            scrollEventThrottle={16}
            style={{ marginTop: '10%', alignSelf: 'center'}}
            horizontal={false}
            contentContainerStyle={{ paddingBottom: '15%' }}
            showsHorizontalScrollIndicator={false}
            data={enterprises}
            keyExtractor={(item) => { return item.id }}
            renderItem={renderEnterprises}
            ListHeaderComponent={renderHeaderList}
            ItemSeparatorComponent={() => <View style={{marginVertical: '5%'}}/>}
            ListEmptyComponent={() => <Text style={{marginVertical: '50%'}}>Not items to show</Text>}
            />

            </Animated.ScrollView>

            <ModalizeComponent
            typeHeader={0}
            typeFooter={typeFooterModal}
            showModal={isModalVisible}
            onClosed={() => onCloseModal()}
            flatListProps={propsFlatListModal}
            funcPrimaryButton={HandlePrimaryButton}
            funcSecondaryButton={HandleSecondaryButton}
            TitleButtonText={MODAL_TITLE_BUTTON_TEXT}
            LeftButtonText={MODAL_LEFT_BUTTON_TEXT}
            RightButtonText={MODAL_RIGHT_BUTTON_TEXT}
            />

            <SnackBar
                Visible={isErrorButton}
                Erro={errorTextButton}
                onDismissSnackBar={() => setIsErrorButton(false)}
                ContainerColor={COLORS.VERMELHO_APP}
                TextColor={COLORS.BRANCO_APP}
            />
        </>
    )
}

export default HomeScreen
