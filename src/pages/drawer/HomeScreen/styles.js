import {StyleSheet,Dimensions} from 'react-native';
import { COLORS } from '../../../constants/colors';
import { FONTS } from '../../../constants/fonts';
const { height, width } = Dimensions.get('window') //Dimensões.

export default StyleSheet.create({
    container:
    {
      flex: 1
      , backgroundColor: COLORS.BRANCO_APP
    }

    , YourLocationContainer:
    {
      flexDirection: 'row'
      , borderWidth: 0.0
      , shadowOffset:
      {
        width: width
        , height: 0
      }
      , shadowColor: 'black'
      , shadowOpacity: 0.0
      , elevation: 0
      , justifyContent: 'space-between'
    }

    , TransparentYourLocationView:
    {
      justifyContent: 'space-between'
      , backgroundColor: 'transparent'
      , marginTop: 25
      , height: 30
      , marginLeft: 16
      , marginRight: 20
      , maxWidth: width * 0.45
      , flexDirection: 'row'
    }

    , YourLocationText:
    {
      fontSize: 22
      , fontFamily: FONTS.Montserrat_Bold
      , color: COLORS.PRETO_APP
    }

    , HeaderList:
    {
      fontSize: 22
      , fontFamily: FONTS.Montserrat_Bold
      , color: COLORS.PRETO_APP
      , marginBottom: '10%'
    }

    , EnterpriseTypeItem:
    {
      height: height * 0.05,
      width: width * 0.8,
      justifyContent: 'center',
      alignSelf: 'center',
      alignItems: 'center',
      backgroundColor: COLORS.PRETO_APP,
      marginVertical: '2%',
      borderRadius: 20
    }

    , EnterpriseTypeItemText:
    {
      fontSize: 22,
      fontFamily: FONTS.Montserrat_Medium,
      color: COLORS.BRANCO_APP,
    }

    , IjobLogoStyle:
    {
      width: width * 0.27
      , height: height * 0.25
      , resizeMode: 'contain'
    }

    , YourLocationSecondView:
    {
      flexDirection: 'row'
      , marginLeft: 5
      , marginRight: 8
      , marginTop: 0
      , borderRadius: 15
      , alignItems: 'center'
  
    }

    , textInput: 
    {
      justifyContent: 'center',
      alignItems: 'center',
      width: width * 0.7,
      height: height * 0.06,
      backgroundColor: COLORS.PRETO_APP,
      borderRadius: 200,
      color: COLORS.BRANCO_APP
    }

    , iconSearch: 
    {
      width: width * 0.2,
      marginRight: '2%',
      marginTop: '9%',
      flexDirection: 'row',
      justifyContent: 'space-between'
    }
  })