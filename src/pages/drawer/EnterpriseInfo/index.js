import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    Image,
    RefreshControl,
    ScrollView
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import { FocusAwareStatusBar } from '../../../components/StatusBar/StatusBar';
import { COLORS } from '../../../constants/colors';
import styles from './styles';
import { VectorIcon } from '../../../utils/VectorIconsUtil';
import { GetBrazillianCurrency } from '../../../utils/CurrencyUtil';
import { getEnterpriseInfo } from '../../../services/EnterprisesServices';
import SnackBar from '../../../components/Snackbar/SnackBar';

const STATUS_BAR_COLOR = COLORS.BRANCO_APP

const URL_BASE_PHOTO = 'https://empresas.ioasys.com.br'
    
const EnterpriseInfo = ({ route }) => {

    const [refreshing, setRefreshing] = useState(false)
    const [enterprise, setEnterprise] = useState([])

    const [isErrorButton, setIsErrorButton] = useState(false)
    const [errorTextButton, setErrorTextButton] = useState(false)

    useEffect(() => {
        Mount()
    }, [])

    function Mount() {
        ListEnterprise()
        setRefreshing(true)
    }

    async function ListEnterprise() {
        const response = await getEnterpriseInfo(route.params.enterpriseId)
        if (response.success) {
            setRefreshing(false)
            setEnterprise(response.enterprise)
        } else {
            setRefreshing(false)
            setIsErrorButton(true)
            setErrorTextButton('Error when searching for the enterprise!')
        }
    }


    function onRefresh() {
        setRefreshing(true)
        Mount()
    }

    const renderRefreshControl = () =>
        <RefreshControl
            tintColor="transparent"
            colors={['transparent']}
            style={{ backgroundColor: 'transparent' }}
            refreshing={refreshing}
            onRefresh={onRefresh}
        />

    return(
        <View style={{flex: 1, backgroundColor: COLORS.BRANCO_APP}}>
            <ScrollView
                style={styles.container}
                keyboardShouldPersistTaps="handled"
                showsVerticalScrollIndicator={false}
                refreshControl={renderRefreshControl()}
            >

            <FocusAwareStatusBar
            backgroundColor={STATUS_BAR_COLOR}
            barStyle="dark-content"
            />

            <Animatable.View animation="slideInUp">

                <View useNativeDriver style={styles.TransparentYourLocationView}>
                    <Text numberOfLines={1} style={styles.YourLocationText}>
                        {enterprise.enterprise_name ?? 'Enterprise'}
                    </Text>
                </View>


                <View useNativeDriver style={{justifyContent: 'center', alignItems: 'center'}}>
                    {
                        enterprise.photo ? <Image source={{ uri: URL_BASE_PHOTO + enterprise.photo  }} style={styles.enterpriseImage} /> :
                        <VectorIcon type={2} IconSize={70} IconName={'image-off'} IconStyle={{justifyContent: 'center', marginVertical: '10%'}} />
                    }
                </View>

                <View useNativeDriver style={styles.itemsContainer}>
                    <View style={styles.itemContainer}>
                        <VectorIcon type={2} IconSize={25} IconName={'city'} IconColor={COLORS.PRETO_APP} />
                        <Text numberOfLines={1} style={styles.infosEnterpriseText}>
                            {enterprise.city ?? ' -- '}
                        </Text>
                    </View>
                    <View style={styles.itemContainer}>
                        <VectorIcon type={2} IconName={'map'} IconColor={COLORS.PRETO_APP} />
                        <Text numberOfLines={1} style={styles.infosEnterpriseText}>
                            {enterprise.country ?? ' -- '}
                        </Text>
                    </View>
                </View>

                <View useNativeDriver style={styles.itemsContainer}>
                    <View style={styles.itemContainer}>
                        <VectorIcon type={2} IconName={'twitter'} IconColor={COLORS.PRETO_APP} />
                        <Text numberOfLines={1} style={styles.infosEnterpriseText}>
                            {enterprise.twitter ?? ' -- '}
                        </Text>
                    </View>
                    <View style={styles.itemContainer}>
                        <VectorIcon type={2} IconName={'facebook'} IconColor={COLORS.PRETO_APP} />
                        <Text numberOfLines={1} style={styles.infosEnterpriseText}>
                            {enterprise.facebook ?? ' -- '}
                        </Text>
                    </View>
                </View>

                <View useNativeDriver style={styles.itemsContainer}>
                    <View style={styles.itemContainer}>
                        <VectorIcon type={2} IconName={'linkedin'} IconColor={COLORS.PRETO_APP} />
                        <Text numberOfLines={1} style={styles.infosEnterpriseText}>
                            {enterprise.linkedin ?? ' -- '}
                        </Text>
                    </View>
                    <View style={styles.itemContainer}>
                        <VectorIcon type={2} IconName={'phone-outline'} IconColor={COLORS.PRETO_APP} />
                        <Text numberOfLines={1} style={styles.infosEnterpriseText}>
                            {enterprise.phone ?? ' -- '}
                        </Text>
                    </View>
                </View>

                <View useNativeDriver style={styles.itemsContainerCenter}>
                    <VectorIcon type={2} IconName={'email-newsletter'} IconColor={COLORS.PRETO_APP} />
                    <Text numberOfLines={1} style={styles.infosEnterpriseText}>
                        {enterprise.email_enterprise ?? ' -- '}
                    </Text>
                </View>

                <View useNativeDriver style={styles.itemsContainerCenter}>
                    <VectorIcon type={2} IconName={'account-cash'} IconColor={COLORS.PRETO_APP} />
                    <Text numberOfLines={1} style={styles.infosEnterpriseText}>
                        {GetBrazillianCurrency(enterprise.share_price) ?? ' -- '}
                    </Text>
                </View>

                <View style={styles.descriptionContainer}>
                    <Text style={styles.detailsEnterpriseText}>
                        {enterprise.description}
                    </Text>
                </View>

            </Animatable.View>

            </ScrollView>
            <SnackBar
                Visible={isErrorButton}
                Erro={errorTextButton}
                onDismissSnackBar={() => setIsErrorButton(false)}
                ContainerColor={COLORS.VERMELHO_APP}
                TextColor={COLORS.BRANCO_APP}
            />
        </View>
    )
}

export default EnterpriseInfo
