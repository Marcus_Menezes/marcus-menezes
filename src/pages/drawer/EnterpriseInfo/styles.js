import {StyleSheet,Dimensions} from 'react-native';
import { COLORS } from '../../../constants/colors';
import { FONTS } from '../../../constants/fonts';
const { height, width } = Dimensions.get('window') //Dimensões.

export default StyleSheet.create({
    
    container:
    {
      flex: 1,
      backgroundColor: COLORS.CINZAINPUT_APP,
      width: width * 0.9,
      marginTop: '10%',
      alignSelf: 'center',
      borderTopRightRadius: 20,
      borderTopLeftRadius: 20
    },
    
    TransparentYourLocationView:
    {
      flex: 1,
      backgroundColor: 'transparent',
      marginTop: '10%',
      marginHorizontal: '10%',
      justifyContent: 'center',
      alignItems: 'center',
    },

    YourLocationText:
    {
      fontSize: 22
      , fontFamily: FONTS.Montserrat_Bold
      , color: COLORS.PRETO_APP
      , marginHorizontal: '2%'
    },

    enterpriseImage:
    {
      width: width * 0.7
      , height: height * 0.3
      , resizeMode: 'contain'
      , borderRadius: 20
    },

    infosEnterpriseContainer:
    {
      marginTop: '10%',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row',
    },

    infosEnterpriseText:
    {
      fontSize: 16,
      fontFamily: FONTS.Montserrat_Medium,
      color: COLORS.PRETO_APP,
      marginHorizontal: '2%',
    },

    detailsEnterpriseText:
    {
      fontSize: 14,
      fontFamily: FONTS.Montserrat_Medium,
      color: COLORS.PRETO_APP,
      textAlign: 'justify',
      marginTop: '5%',
      marginHorizontal: '4%'
    },

    itemContainer:
    {
      flexDirection: 'row', 
      justifyContent: 'center', 
      alignItems: 'center'
    },

    itemsContainer:
    {
      flexDirection: 'row', 
      justifyContent: 'space-around', 
      marginTop: '10%'
    },

    itemsContainerCenter:
    {
      flexDirection: 'row', 
      justifyContent: 'center', 
      marginTop: '10%'
    },

    descriptionContainer:
    {
      alignItems: 'center', 
      justifyContent: 'center', 
      marginVertical: '10%'
    },
  })