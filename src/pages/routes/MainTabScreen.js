import {LogBox} from 'react-native'
LogBox.ignoreAllLogs('disable')
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import React from 'react';
import SplashScreenElement from 'react-native-splash-screen';
import HomeScreen from '../drawer/HomeScreen/index';
import EnterpriseInfo from '../drawer/EnterpriseInfo';

try { SplashScreenElement.hide(); } catch (e) { }

const HomeStack = createStackNavigator();

const MainTabScreen = () => {
  
  return (
    <HomeStack.Navigator  screenOptions={{headerShown: false,...TransitionPresets.SlideFromRightIOS}}>

      <HomeStack.Screen name="Home" component={ HomeScreen } />    
      <HomeStack.Screen name="EnterpriseInfo" component={ EnterpriseInfo } />    

    </HomeStack.Navigator>

)};

export default MainTabScreen;