import React, { useEffect, useState } from 'react';
import * as Animatable from 'react-native-animatable';
import Feather from 'react-native-vector-icons/Feather';
import {
View,
Text,
TouchableOpacity,
TextInput,
StatusBar,
Dimensions,
Image,
KeyboardAvoidingView,
ScrollView
} from 'react-native';

import { AuthContext } from '../../../context/context';
import { COLORS } from '../../../constants/colors';
import { FONTS } from '../../../constants/fonts';
import SplashScreenElement from 'react-native-splash-screen';
import SnackBar from '../../../components/Snackbar/SnackBar';
import AwesomeButton from '../../../components/AnimatedButtonCartman/themes/cartman';
import { VectorIcon } from '../../../utils/VectorIconsUtil';
import { singInUser } from '../../../services/LoginServices';
import styles from './styles';

const { height, width } = Dimensions.get('window')

try { SplashScreenElement.hide(); } catch (e) { }

const SAVE_ICON = "send-circle-outline"
const IMAGE_LOGO = '../../../assets/logo_ioasys.png'

const SignInScreen = () => {

    const [data, setData] = React.useState({
        email: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidEmail: false,
        isValidPassword: false,
    });

    useEffect(() => {
        if(data.isValidEmail && data.isValidPassword)
        {
            setShouldDisableButton(false)
        }
        else {
            setShouldDisableButton(true)
        }
    }, [data.email,data.password])

    const [isErrorButton, setIsErrorButton] = useState(false)
    const [ErrorTextButton, setErrorTextButton] = useState('')
    const [ShouldDisableButton, setShouldDisableButton] = useState(true)

    const { signIn } = React.useContext(AuthContext);

    function validateEmail(email) {
        const validation = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return validation.test(String(email).toLowerCase());
    }

    const textInputChange = (val) => {
        const validate = validateEmail(val)
        if (validate) {
            setData({
                ...data,
                email: val,
                check_textInputChange: true,
                isValidEmail: true
            });
        } else {
            setData({
                ...data,
                email: val,
                check_textInputChange: false,
                isValidEmail: false
            });
        }
    }

    const handlePasswordChange = (val) => {
        if (val.trim().length >= 8) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });
        }
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const alertFutureFeature = () => {
        setErrorTextButton('This feature is still in development, please try again later!')
        setIsErrorButton(true)
    }

    const login = async (email, password, next) => {
        const loginResponse = await singInUser(email, password)

        if (loginResponse.success)
        {
            if (loginResponse.headers) {
                foundUser = {
                    uid: loginResponse.headers.uid,
                    client: loginResponse.headers.client,
                    userToken: loginResponse.headers.acessToken,
                } 
                signIn(foundUser);
            } else {
                setErrorTextButton('Error logging in, please try again later!')
                setIsErrorButton(true)
            }
            next()
        } else {
            setErrorTextButton(loginResponse.errors)
            setIsErrorButton(true)
            next()
        }
        
    }

    const loginHandle = (email, password, next) => {
        if (data.isValidPassword, data.isValidEmail) {
            login(email, password, next)
        } else {
            setErrorTextButton('Error your information is incorrect!')
            setIsErrorButton(true)
        }
    }

    return (
        <>
        <ScrollView style={styles.container}>
        
        
            <StatusBar backgroundColor={ COLORS.BRANCO_APP} barStyle="light-content" />

            <View style={{flexDirection: 'column', justifyContent:'space-around', maxHeight: height - 30, minHeight: height - 30}}>

            <KeyboardAvoidingView behavior="position" enabled>
                <Animatable.View useNativeDriver animation='rubberBand' iterationCount={1} style={{ width: width, alignItems: 'center' }}>
                    <Image
                        style={{ height: height / 3.5, width: width * 0.8, resizeMode: 'contain' }}
                        source={require(IMAGE_LOGO)} />
                </Animatable.View>

                <Animatable.View animation="fadeInUpBig" style={styles.footer}>
                   
                    <View style={styles.card}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingHorizontal: 5 }}>

                            <VectorIcon type={2} IconName="account-arrow-right"/>

                            <TextInput
                                style={{ color: COLORS.BRANCO_APP, flex: 1, fontFamily: FONTS.Montserrat_Medium }}
                                placeholder="Email"
                                placeholderTextColor={COLORS.BRANCO_APP}
                                keyboardType="email-address"
                                autoCapitalize="none"
                                onChangeText={(val) => textInputChange(val)}
                            />

                            {
                                data.check_textInputChange &&
                                    <Animatable.View animation="bounceIn">
                                        <Feather name="check-circle" color={COLORS.BRANCO_APP} size={20} style={{ paddingHorizontal: 5 }} />
                                    </Animatable.View>
                            }


                        </View>
                    </View>

                    {
                        !data.isValidEmail &&
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>Use the same email address as your registration.</Text>
                            </Animatable.View>
                    }


                    <View style={styles.card}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingHorizontal: 5 }}>

                            <VectorIcon type={2} IconName="lock"/>

                            <TextInput
                                style={{ color: COLORS.BRANCO_APP, flex: 1, fontFamily: FONTS.Montserrat_Medium }}
                                placeholder="Senha"
                                placeholderTextColor={COLORS.BRANCO_APP}
                                secureTextEntry={data.secureTextEntry ? true : false}
                                underlineColorAndroid='transparent'
                                autoCapitalize="none"
                                onChangeText={(val) => handlePasswordChange(val)}
                            />
                            <TouchableOpacity onPress={updateSecureTextEntry}>
                                {
                                    data.secureTextEntry ?
                                        <VectorIcon
                                            type={2}
                                            IconName="eye"
                                        />
                                        :
                                        <VectorIcon
                                            type={2}
                                            IconName="eye-off"
                                        />
                                }
                            </TouchableOpacity>
                        </View>
                    </View>


                    {
                        !data.isValidPassword &&
                            <Animatable.View animation="fadeInLeft" duration={500}>
                                <Text style={styles.errorMsg}>Your password must contain at least 8 characters.</Text>
                            </Animatable.View>
                    }




                    <View style={styles.button}>
            
                        <AwesomeButton
                            disabled={ShouldDisableButton}
                            activityColor={COLORS.BRANCO_APP}
                            width={width * 0.90}
                            type={'secondary'}
                            progress
                            onPress={next => loginHandle(data.email, data.password, next)}>

                            <View style={styles.ButtonContentContainer}>
                            <Text style={styles.ButtonText}>Login</Text>
                                <VectorIcon
                                    type={2}
                                    IconName={SAVE_ICON}
                                    IconColor={COLORS.BRANCO_APP}
                                    IconSize={21} />
                            </View>

                        </AwesomeButton>
                    </View>
                    
                </Animatable.View>
                </KeyboardAvoidingView>
                
                <Animatable.View animation="fadeInUpBig" style={{flexDirection: 'row',alignSelf:'center', justifyContent: 'space-around', width: '100%'}}>

                        <TouchableOpacity style={styles.signIn} onPress={alertFutureFeature}>
                            <Text style={styles.textSign}>Recover Password</Text>
                            <VectorIcon type={2} IconName={'reload'} IconColor={COLORS.PRETO_APP} />
                        </TouchableOpacity>


                        <TouchableOpacity onPress={alertFutureFeature} style={styles.signIn}>
                            <Text style={styles.textSign}>Sign up</Text>
                            <VectorIcon type={2} IconColor={COLORS.PRETO_APP} IconName={'account-plus'}/>
                        </TouchableOpacity>
                </Animatable.View>   
            </View>  
        </ScrollView>
        
        <SnackBar
          Visible={isErrorButton}
          Erro={ErrorTextButton}
          onDismissSnackBar={() => setIsErrorButton(false)}
          ContainerColor={COLORS.VERMELHO_APP}
          TextColor={COLORS.BRANCO_APP}
        />
        </>
    );
};

export default SignInScreen;