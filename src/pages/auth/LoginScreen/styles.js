import {StyleSheet,Dimensions} from 'react-native';
import { COLORS } from '../../../constants/colors';
import { FONTS } from '../../../constants/fonts';
const { width } = Dimensions.get('window') //Dimensões.

export default styles = StyleSheet.create({
    container:
    {
        backgroundColor:  COLORS.BRANCO_APP,
    },
    header:
    {
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer:
    {
        backgroundColor:  COLORS.BRANCO_APP
        , borderTopLeftRadius: 0
        , borderTopRightRadius: 0
        , paddingHorizontal: 20
        , paddingVertical: 30
    },
    text_header:
    {
        color: '#fff'
        , fontFamily: FONTS.Montserrat_Medium
        , fontSize: 30
    },
    text_footer:
    {
        color: '#05375a'
        , fontSize: 18
    },
    buttonContainer:
    {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 5,
        width: width * 0.80,
        borderRadius: 50,
        borderWidth: 0.7,
        borderColor: '#FFF'

    },
    action:
    {
        flexDirection: 'row'
        , backgroundColor: '#FFF'
        , borderTopRightRadius: 10
        , borderTopLeftRadius: 10
        , borderBottomLeftRadius: 10
        , borderBottomRightRadius: 10
        , marginTop: 10
        , borderBottomWidth: 1
        , borderBottomColor: '#f2f2f2'
        , alignItems: 'center'
        , paddingHorizontal: 5
        , paddingBottom: 5
        , alignContent: "center"
        , justifyContent: "center"
        , alignSelf: "center"
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
        fontFamily: FONTS.Montserrat_Bold
    },
    errorMsg: {
        color: COLORS.VERMELHO_APP,
        fontSize: 12,
        paddingVertical: 5,
        alignSelf: 'center'
        , fontFamily: FONTS.Montserrat_Medium
    },
    button: {
        alignItems: 'center',
        marginVertical: '10%'
    },
    signIn: {
        width: '40%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderColor:  COLORS.PRETO_APP,
        flexDirection: 'row',
        borderWidth: 1.5,
        padding: '5%',
    },
    textSign: {
        fontSize: 14,
        fontFamily: FONTS.Montserrat_Medium,
        color: COLORS.PRETO_APP,
        width: '100%',
    }
    ,
    card: {

        shadowColor: '#00000021',
        shadowOffset: {
            width: 2
        },
        shadowOpacity: 0.5,
        shadowRadius: 4,
        marginVertical: 5,
        backgroundColor: COLORS.PRETO_APP,
        borderColor: '#4739ea',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginTop: 5,
        marginRight: 5,
        marginLeft: 5,


    },
    ButtonContentContainer:
    {
        flexDirection: 'row'
    }
    , ButtonText:
    {
        fontFamily: FONTS.Montserrat_Black
        , color: COLORS.BRANCO_APP
    }
    , ButtonTextError:
    {
        fontFamily: FONTS.Montserrat_Black
        , color: COLORS.VERMELHO_APP
    }


});