import React from "react";
import PropTypes from "prop-types";
import AwesomeButton from "../Button";
import SocialTypes from "./social";
import {COLORS} from "../../../constants/colors"
const COMMON = {
  borderRadius: 8,
  height: 55,
  activityColor: "#FFE11D",
  raiseLevel: 8
};

const SOCIAL_TYPES = SocialTypes(COMMON);

const BUTTONS = {
  primary: {
    ...COMMON,
    backgroundColor: COLORS.ROXO_APP,
    backgroundDarker: COLORS.ROXO_APP,
    textColor: "#FFF",
    borderWidth: 1,
    borderColor: COLORS.VERDE_APP
  },
  secondary: {
    ...COMMON,
    backgroundColor: COLORS.PRETO_APP,
    backgroundDarker: COLORS.PRETO_APP,
    textColor: COLORS.BRANCO_APP,
    borderWidth: 1,
    borderColor: COLORS.BRANCO_APP
  },
  third: {
    ...COMMON,
    backgroundColor: COLORS.ROXO_APP,
    backgroundDarker: COLORS.BRANCO_APP,
    textColor: "#FFF",
    borderWidth: 1,
    borderColor: COLORS.BRANCO_APP
  },
  anchor: {
    ...COMMON,
    backgroundColor: COLORS.VERDE_APP,
    backgroundDarker: COLORS.VERDE_APP,
    textColor: COLORS.PRETO_APP,
    backgroundProgress: COLORS.ROXO_APP,
    borderWidth: 1,
    borderColor: COLORS.ROXO_APP
  },
  another: {
    ...COMMON,
    backgroundColor: COLORS.BRANCO_APP,
    backgroundDarker: COLORS.ROXO_APP,
    textColor: COLORS.PRETO_APP,
    backgroundProgress: COLORS.ROXO_APP,
    borderWidth: 1,
    borderColor: COLORS.ROXO_APP
  },
  disabled: {
    ...COMMON,
    backgroundColor: "#DFDFDF",
    backgroundDarker: "#CACACA",
    textColor: "#B6B6B6"
  },
  primaryFlat: {
    backgroundColor: "rgba(0, 0, 0, 0)",
    backgroundDarker: "rgba(0, 0, 0, 0)",
    backgroundShadow: "rgba(0, 0, 0, 0)",
    raiseLevel: 0,
    borderRadius: 0
  },
  ...SOCIAL_TYPES
};

const SIZE = {
  small: {
    width: 120,
    height: 42,
    textSize: 12
  },
  medium: {
    width: 200,
    height: 55
  },
  large: {
    width: 250,
    height: 60,
    textSize: 16
  }
};

function theme(props) {
  const { disabled, type, size } = props;
  const styles = disabled ? BUTTONS.disabled : BUTTONS[type];
  const sizeObj = size ? SIZE[size] : {};
  return <AwesomeButton {...styles} {...sizeObj} {...props} />;
}

theme.propTypes = {
  type: PropTypes.string,
  disabled: PropTypes.bool,
  size: PropTypes.string
};

theme.defaultProps = {
  type: "primary",
  disabled: false,
  size: null
};

export default theme;
