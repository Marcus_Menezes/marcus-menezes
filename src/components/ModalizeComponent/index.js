import React, { useEffect, useRef } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { COLORS } from '../../constants/colors';
import styles from './styles';
import * as Animatable from 'react-native-animatable';
import PrimaryButton from '../Button/PrimaryButton';
import SecondaryButton from '../Button/SecondaryButton';
import DoubleButtons from '../Button/DoubleButtons';

const { height, width } = Dimensions.get('window')

export const ModalizeComponent = ({ 
  showModal,
  typeHeader,
  typeFooter,
  snapPoint,
  titleTextHeader = '',
  onClosed = () => {},
  customHeader = () => {},
  customFooter = () => {},
  funcPrimaryButton = () => {},
  funcSecondaryButton = () => {},
  TitleButtonText = '',
  IconSingleButton = 'check',
  LeftButtonText = 'Sim',
  RightButtonText = 'Não',
  withHandle = false,
  flatListProps,
}) => {

  const modalizeRef = useRef(null);

  useEffect(() => {
    if (showModal)
      onOpen()
    else onClose()
  }, [showModal])

  const onOpen = () => {
    modalizeRef.current?.open();
  };

  const onClose = () => {
    modalizeRef.current?.close();
  };

  const _renderModalHeader = () => {
    switch (typeHeader) {
      
      // Custom Header
      case 0:
        return customHeader()
      
      // Texto no Header (Titulo)
      case 1:
        return (
          <View style={styles.ModalHeaderContainer}>
            <Text style={styles.ModalHeaderText}>{titleTextHeader}</Text>
          </View>
        )

      // Botão Unico no Header
      case 2:
        return (
          <Animatable.View animation={'slideInLeft'} useNativeDriver style={styles.ButtonHeaderContainer}>
              <SecondaryButton
                useNativeDriver={true}
                TextAnimation={'fadeIn'}
                TitleText={TitleButtonText}
                Icon={IconSingleButton}
                onPressPositive={() => funcPrimaryButton()}
              />
          </Animatable.View>
        )  
    
      default:
        break;
    }
  }

  const _renderModalFooter = () => {
    switch (typeFooter) {

      // Custom Footer
      case 0:
        return customFooter()

      // Botões Editar / Excluir
      case 1:
        return (
          <View style={styles.ButtonsContainer}>
            <Animatable.View animation={'slideInLeft'} useNativeDriver>
                <PrimaryButton
                  useNativeDriver={true}
                  TextAnimation={'fadeIn'}
                  TitleText={"Editar"}
                  Icon={'circle-edit-outline'}
                  onPressPositive={() => funcPrimaryButton()}
                />
              </Animatable.View>
              <Animatable.View animation={'slideInRight'} useNativeDriver style={{marginTop: '5%'}}>
                <SecondaryButton
                  useNativeDriver={true}
                  TextAnimation={'fadeIn'}
                  TitleText={"Excluir"}
                  Icon={'delete-circle-outline'}
                  onPressPositive={() => funcSecondaryButton()}
                />
              </Animatable.View>
          </View>
        )

      // Botões Duplos de Sim e Não
      case 2:
        return (
          <DoubleButtons
              LeftButtonText={LeftButtonText}
              RightButtonText={RightButtonText}
              onPressLeft={() => funcPrimaryButton()}
              onPressRight={() => funcSecondaryButton()}
              TitleText={TitleButtonText}
            />
        )

      // Botão Unico
      case 3:
        return (
          <Animatable.View animation={'slideInUp'} useNativeDriver style={styles.ButtonsContainer}>
              <PrimaryButton
                useNativeDriver={true}
                TextAnimation={'fadeIn'}
                TitleText={TitleButtonText}
                Icon={IconSingleButton}
                onPressPositive={() => funcPrimaryButton()}
              />
          </Animatable.View>
        )
    
      default:
        break;
    }
  }

    return (
      <>
        <Modalize 
        ref={modalizeRef} 
        HeaderComponent={_renderModalHeader}
        FooterComponent={_renderModalFooter}
        modalStyle={styles.Modal}
        flatListProps={flatListProps}
        onClosed={onClosed}
        closeOnOverlayTap={typeFooter === 4 ? false: true}
        panGestureEnabled={typeFooter === 4 ? false: true}
        withHandle={withHandle}
        snapPoint={snapPoint}
        adjustToContentHeight
        /> 
      </>
    );
}