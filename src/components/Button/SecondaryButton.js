import React from 'react';
import PropTypes from "prop-types";
import * as Animatable from 'react-native-animatable';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {StyleSheet,Text,TouchableOpacity } from 'react-native';

import {COLORS} from '../../constants/colors';
import {FONTS} from '../../constants/fonts';

export default class SecondaryButton extends React.Component 
{
    onPressPositive = () => { this.props.onPressPositive(); };

    render()
    {
        return(
            <TouchableOpacity disabled={this.props.Disabled} style={[styles.Button,{opacity: this.props.Disabled ? 0.5 : 1}]} onPress={this.onPressPositive}>                
                <Animatable.View animation={this.props.TextAnimation} useNativeDriver={this.props.useNativeDriver} style={{flexDirection: 'row', alignItems: 'center'}}>                    
                    <Text style={styles.text}>{this.props.TitleText}</Text>
                    <MaterialCommunityIcons name={this.props.Icon} color={COLORS.ROXO_APP} size={20}/>
                </Animatable.View>
            </TouchableOpacity>
        )
    }
}

SecondaryButton.propTypes = {    
    TitleText: PropTypes.string
    ,Icon: PropTypes.string
    ,TextAnimation: PropTypes.string
    ,useNativeDriver: PropTypes.bool
    ,Disabled: PropTypes.bool
    ,onPressPositive : PropTypes.func    
}

const styles = StyleSheet.create({
    text:
    {
        fontFamily: FONTS.FonteProTrial
        ,color:COLORS.ROXO_APP
        ,fontSize:16
        ,marginRight:10
    }
    ,Button:
    {
        alignItems:'center'
        ,backgroundColor:COLORS.BRANCO_APP
        ,borderWidth:0.5
        ,borderColor:COLORS.ROXO_APP
        ,marginHorizontal:'5%'
        ,paddingVertical:'2%'
        ,borderRadius:5

    }
})