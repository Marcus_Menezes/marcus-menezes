import React from 'react';
import PropTypes from "prop-types";
import * as Animatable from 'react-native-animatable';
import {StyleSheet,Text,TouchableOpacity,View } from 'react-native';

import {COLORS} from '../../constants/colors';
import {FONTS} from '../../constants/fonts';

const DoubleButtons = ({
    onPressLeft, 
    onPressRight, 
    TitleText, 
    LeftButtonText = 'Sim', 
    RightButtonText='Não', 
    useNativeDriver = true, 
    TextAnimationTitle = 'fadeIn',
    TextAnimationLeftButton = 'slideInLeft',
    TextAnimationRightButton = 'slideInRight',
}) => 
{
    {
        return(
        <View>
            <Animatable.Text animation={TextAnimationTitle} delay={500} style={styles.TitleText}>{TitleText}</Animatable.Text>
            <View style={styles.container}>
            
            <Animatable.View animation={TextAnimationLeftButton} useNativeDriver={useNativeDriver} style={styles.Button} >
                <TouchableOpacity onPress={onPressLeft}>                     
                    <Text style={styles.text}>{LeftButtonText}</Text>
                </TouchableOpacity>
            </Animatable.View>
            
                              
            <Animatable.View animation={TextAnimationRightButton} useNativeDriver={useNativeDriver} style={styles.Button}>
                <TouchableOpacity onPress={onPressRight}>                      
                    <Text style={styles.text}>{RightButtonText}</Text>
                </TouchableOpacity>
            </Animatable.View>
            
            </View>
        </View>
        )
    }
}

DoubleButtons.propTypes = {    
    TitleText: PropTypes.string
    ,Icon: PropTypes.string
    ,TextAnimation: PropTypes.string
    ,useNativeDriver: PropTypes.bool
    ,Disabled: PropTypes.bool
    ,onPressPositive : PropTypes.func    
}

export default DoubleButtons;

const styles = StyleSheet.create({
    container:
    {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    }
    ,text:
    {
        fontFamily: FONTS.FonteProTrial
        ,color:COLORS.BRANCO_APP
        ,fontSize:16
        ,justifyContent: 'center'
    }
    ,TitleText:
    {
        fontFamily: FONTS.Montserrat_Bold
        ,color:COLORS.PRETO_APP
        ,fontSize: 17
        ,alignSelf:'center'
        ,marginVertical: '2%'
        ,textAlign: 'center'
    }
    ,Button:
    {
        alignItems:'center'
        ,width: '35%'
        ,backgroundColor:COLORS.PRETO_APP
        ,marginHorizontal:'5%'
        ,marginVertical: '5%'
        ,paddingVertical:'2%'  
        ,borderRadius: 5 
    }
    ,disabledButton:
    {
        alignItems:'center'
        ,backgroundColor:COLORS.CINZAINPUT_APP
        ,marginHorizontal:'5%'
        ,paddingVertical:'2%'
        ,borderRadius:5
        ,opacity: 1
    }
})