import React from 'react';
import PropTypes from "prop-types";
import * as Animatable from 'react-native-animatable';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {StyleSheet,Text,TouchableOpacity } from 'react-native';

import {COLORS} from '../../constants/colors';
import {FONTS} from '../../constants/fonts';

export default class PrimaryButton extends React.Component 
{
    onPressPositive = () => { this.props.onPressPositive(); };

    render()
    {
        return(
            <TouchableOpacity 
            disabled={this.props.Disabled} 
            style={this.props.Disabled ? styles.disabledButton : styles.Button} 
            onPress={this.onPressPositive}>                
                <Animatable.View animation={this.props.TextAnimation} useNativeDriver={this.props.useNativeDriver} style={{flexDirection: 'row', alignItems: 'center'}}>                    
                    <Text style={styles.text}>{this.props.TitleText}</Text>
                    <MaterialCommunityIcons name={this.props.Icon} color={COLORS.BRANCO_APP} size={20}/>
                </Animatable.View>
            </TouchableOpacity>
        )
    }
}

PrimaryButton.propTypes = {    
    TitleText: PropTypes.string
    ,Icon: PropTypes.string
    ,TextAnimation: PropTypes.string
    ,useNativeDriver: PropTypes.bool
    ,Disabled: PropTypes.bool
    ,onPressPositive : PropTypes.func    
}

const styles = StyleSheet.create({
    text:
    {
        fontFamily: FONTS.FonteProTrial
        ,color:COLORS.BRANCO_APP
        ,fontSize:16
        ,marginRight:10
    }
    ,Button:
    {
        alignItems:'center'
        ,backgroundColor:COLORS.ROXO_APP
        ,marginHorizontal:'5%'
        ,paddingVertical:'2%'
        ,borderRadius:5

    }
    ,disabledButton:
    {
        alignItems:'center'
        ,backgroundColor:COLORS.CINZAINPUT_APP
        ,marginHorizontal:'5%'
        ,paddingVertical:'2%'
        ,borderRadius:5
        ,opacity: 1
    }
})