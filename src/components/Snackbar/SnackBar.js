import PropTypes from "prop-types";
import React, { useState } from 'react';
import { Dimensions, Text } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { Snackbar } from 'react-native-paper';
import { COLORS } from '../../constants/colors';
import { FONTS } from '../../constants/fonts';

const { height, width } = Dimensions.get('window')

const duration = 1200;

const SnackBar = ({
  Visible, 
  Erro,
  onDismissSnackBar,
  ContainerColor = COLORS.PRETO_APP,
  TextColor = COLORS.BRANCO_APP,
}) => {

  const [Animation, setAnimation] = useState('fadeInUp')
  
  return (
    <Animatable.View  style={{zIndex:99 }} animation={Animation} duration={1000} useNativeDriver={true}>
      <Snackbar
        visible={Visible}
        onDismiss={() => {
          setAnimation("fadeOutDown")
          
          setTimeout(() => {
            onDismissSnackBar()
            setAnimation("fadeInUp")
          }, duration);
        }}
        duration={duration}          
        style={{ backgroundColor: ContainerColor, zIndex:99}}>
        <Text style={{ fontFamily: FONTS.Montserrat_ExtraBold, color: TextColor, fontSize:14,textAlign: 'justify'}}>{Erro}</Text>
      </Snackbar>
    </Animatable.View>
    
  )

}

Snackbar.propTypes = {
  Visible: PropTypes.bool
  , Erro: PropTypes.string
  , onDismissSnackBar: PropTypes.func
  , ContainerColor: PropTypes.string
  , TextColor: PropTypes.string
}

export default SnackBar;