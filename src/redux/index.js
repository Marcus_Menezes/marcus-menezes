import { applyMiddleware, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga';
import {composeWithDevTools} from 'redux-devtools-extension'

import reducers from './ducks';
import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware()

const compose = composeWithDevTools({});

const store = createStore(reducers, compose(applyMiddleware(sagaMiddleware)))

sagaMiddleware.run(sagas);

export default store
