import { put } from "@redux-saga/core/effects";

import { Creators as EnterpriseActions } from '../ducks/enterprises'

export function* getEnterprises() {
    try {
        const response = await getListEnterprises()

        if (response.enterprises.length > 0) {
            put(EnterpriseActions.setEnterprises(response.enterprise))
            const allTypes = response.enterprises.map(item => item.enterprise_type)
            const allTypesFiltered = distinctFunc(allTypes, ["id"])
            put(EnterpriseActions.setEnterprises(allTypesFiltered))
        } else {
            put(EnterpriseActions.setEnterprises('Error when searching for Enterprises!'))
        }

    } catch (error) {
        put(EnterpriseActions.setEnterprises(error))
    }
}