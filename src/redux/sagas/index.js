import { all, takeLatest } from 'redux-saga/effects'
import { Types as LoginTypes } from '../ducks/login'
import { signIn } from './login'

export default function* rootSaga() {
    yield all([
        takeLatest(LoginTypes.SIGN_IN, signIn),
    ])
}