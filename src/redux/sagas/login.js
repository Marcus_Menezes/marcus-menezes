import { call } from "@redux-saga/core/effects";
import { singInUser } from "../../services/LoginServices";

import { Creators as LoginActions } from '../ducks/login'

export function* signIn(action) {
    const { email, password } = action.payload;
    try {
        
        const response = yield call(singInUser, email, password)

        if (response.success) { 
            put(LoginActions.signInSucess(response))
        } else {
            put(LoginActions.signInError(response.errors[0]))
        }

    } catch (error) {
        put(LoginActions.signInError(error))
    }
}