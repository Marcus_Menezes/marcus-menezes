export const Types = {
    GET_ENTERPRISES: 'enterprises/GET_ENTERPRISES',
    SET_ENTERPRISES: 'enterprises/SET_ENTERPRISES',
    SET_ENTERPRISES_TYPES: 'enterprises/SET_ENTERPRISES_TYPES',
    GET_ENTERPRISES_ERROR: 'enterprises/SIGN_IN_ERROR',
}

export const INITIAL_STATE = {
    list: null,
    types: null,
    loading: false,
    error: null,
}

export default function enterprises(state = INITIAL_STATE, action) {
    switch (action.type) {
        case Types.GET_ENTERPRISES:
            return {
                ...state,
                loading: true
            }
        case Types.SET_ENTERPRISES:
            return {
                ...state,
                loading: INITIAL_STATE.loading,
                list: action.payload.list
            }
        case Types.SET_ENTERPRISES_TYPES:
            return {
                ...state,
                loading: INITIAL_STATE.loading,
                types: action.payload.types
            }
        case Types.GET_ENTERPRISES_ERROR:
            return {
                ...state,
                loading: INITIAL_STATE.loading,
                error: action.payload.error, 
            }
    
        default:
            return state;
    }
}

export const Creators = {

    getEnterprises: () => ({
        type: Types.GET_ENTERPRISES
    }),
    setEnterprises: list => ({
        type: Types.SET_ENTERPRISES
        ,payload: {
            list,
        }
    }),
    setEnterprisesType: types => ({
        type: Types.SET_ENTERPRISES_TYPES
        ,payload: {
            types,
        }
    }),
    getEnterprisesError: error => ({
        type: Types.SIGN_IN_ERROR
        ,payload: {
            error,
        }
    }),
}