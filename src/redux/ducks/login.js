export const Types = {
    SIGN_IN: 'login/SIGN_IN',
    SIGN_IN_SUCESS: 'login/SIGN_IN_SUCCESS',
    SIGN_IN_ERROR: 'login/SIGN_IN_ERROR',
}

export const INITIAL_STATE = {
    credentials: {
        accessToken: null,
        client: null,
        uid: null,
    },
    loading: false,
    error: null,
}

export default function login(state = INITIAL_STATE, action) {
    switch (action.type) {
        case Types.SIGN_IN:
            return {
                ...state,
                loading: true
            }
        case Types.SIGN_IN_SUCCESS:
            return {
                ...state,
                loading: INITIAL_STATE.loading,
                credentials: {
                    accessToken: action.payload.accessToken,
                    client: action.payload.client,
                    uid: action.payload.uid,
                }
            }
        case Types.SIGN_IN_ERROR:
            return {
                ...state,
                loading: INITIAL_STATE.loading,
                error: action.payload.error, 
            }
    
        default:
            return state;
    }
}

export const Creators = {

    signIn: () => ({
        type: Types.SIGN_IN
    }),
    signInSucess: () => ({
        type: Types.SIGN_IN_SUCCESS
    }),
    signInError: error => ({
        type: Types.SIGN_IN_ERROR
        ,payload: {
            error,
        }
    }),
}