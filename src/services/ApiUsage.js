import { GetUser } from "../utils/AsyncUtil";

export const ApiUrl = "https://empresas.ioasys.com.br/api/v1/"

export async function UseAPI(ControllerMetodo, request, type = 1)
{
    const getuser = await GetUser() 
    let response = null
    let headers = null
    let status = null

    if(type === 1) {
        response = await fetch(ApiUrl + ControllerMetodo,
            {
                method: 'GET',
                headers: 
                { 
                    Accept: 'application/json', 
                    'Content-Type': 'application/json',
                    'access-token': getuser?.userToken,
                    'client': getuser?.client,
                    'uid': getuser?.uid
                },
            });
        status = response.status ;
    } else {
        response = await fetch(ApiUrl + ControllerMetodo,
            {
                method: 'POST',
                headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
                body: request,
            });
        
        const acessToken = response.headers.get('access-token')
        const client = response.headers.get('client')
        const uid = response.headers.get('uid')
        if (acessToken && client && uid)
            headers = { acessToken, client, uid }
    }

    const responseJson = await response.json();

    if (status) {
        const completeResponse = { ...responseJson, status }
        return completeResponse;
    }

    if (!headers)
        return responseJson;

    const completeResponse = { ...responseJson, headers}

    return completeResponse;
}

export default {UseAPI};