import { UseAPI } from './ApiUsage';

export async function getListEnterprises() {
    try {
        const response = UseAPI('enterprises', null, 1)

        return response
    }
    catch (error) {
        console.log(error)
    }
}

export async function getEnterpriseByType(type) {
    try {
        const response = UseAPI(`/enterprises?enterprise_types=${type}`, null, 1)

        return response
    }
    catch (error) {
        console.log(error)
    }
}

export async function getEnterpriseByName(name) {
    try {
        const response = UseAPI(`/enterprises?name=${name}`, null, 1)

        return response
    }
    catch (error) {
        console.log(error)
    }
}

export async function getEnterpriseInfo(id) {
    try {
        const response = UseAPI(`enterprises/${id}`, null, 1)

        return response
    }
    catch (error) {
        console.log(error)
    }
}