import { UseAPI } from './ApiUsage';

export async function singInUser(email, password) {

    const request = JSON.stringify({
        "email": email,
        "password": password
    })

    const response = UseAPI('users/auth/sign_in', request, 2)

    return response
}