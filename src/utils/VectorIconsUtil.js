import React from 'react'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import Entypo from 'react-native-vector-icons/Entypo'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import {COLORS} from '../constants/colors'

export const VectorIcon = ({type =1, IconName, IconColor = COLORS.BRANCO_APP, IconSize = 20,IconStyle={paddingHorizontal:5}}) => {
    switch (type) { 
        case 1:
            return (
                <FontAwesome name={IconName} color={IconColor} size={IconSize} style={IconStyle} />
            )

        case 2:
            return (
                <MaterialCommunityIcons name={IconName} color={IconColor} size={IconSize} style={IconStyle} />
            )

        case 3:
            return (
                <Ionicons name={IconName} color={IconColor} size={IconSize} style={IconStyle} />
            )
        case 4:
            return (
                <MaterialIcons name={IconName} color={IconColor} size={IconSize} style={IconStyle} />
            )
        case 5:
            return (
                <Feather name={IconName} color={IconColor} size={IconSize} style={IconStyle} />
            )
        case 6:
            return (
                <FontAwesome5 name={IconName} color={IconColor} size={IconSize} style={IconStyle} />
            )
        case 7:
            return (
                <Entypo name={IconName} color={IconColor} size={IconSize} style={IconStyle} />
            )
        case 8:
            return (
                <SimpleLineIcons name={IconName} color={IconColor} size={IconSize} style={IconStyle} />
            )
        default:
            return (
                <Feather name={IconName} color={IconColor} size={IconSize} style={IconStyle} />
            )            
    }
}




export const MaterialCommunityIconsTags = {
    
}

export const FontAwesomeTags = {

}