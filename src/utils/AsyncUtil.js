import AsyncStorage from '@react-native-community/async-storage';
const STORAGE_USER = '@User'

export async function SetUser(user)
{
    AsyncStorage.setItem(STORAGE_USER, JSON.stringify(user), (err)=> 
    {
        if(err) { throw err; }

    }).catch((err)=> 
    {
        console.log(error)
    });
}

export async function GetUser() 
{
    try 
    {
        const value = await AsyncStorage.getItem(STORAGE_USER);
        
        if (value !== null) 
        { 
            return JSON.parse(value)
        }
        return null;
    }
    catch (error) {console.log(error)}
}

export default { SetUser, GetUser };
