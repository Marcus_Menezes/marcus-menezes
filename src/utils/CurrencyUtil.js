export function GetBrazillianCurrency(Price = 0) {
    return 'R$' + Price.toFixed(2).replace(',', '.').replace('.', ',')
}
