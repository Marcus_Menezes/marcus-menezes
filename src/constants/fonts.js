const FONTS=
{
    FonteProLight:'Cocogoose Pro Light-trial'
    ,FonteProSemi:'Cocogoose Pro Semilight-trial'
    ,FonteProThin:'Montserrat-Thin'
    ,FonteProUltra:'Cocogoose Pro Ultralight-trial'
    ,FonteProTrial:'Montserrat-Black'
    ,FonteProItalicTrial:'Cocogoose Pro Italic-trial'

    ,Montserrat_Black:'Montserrat-Black'
    ,Montserrat_BlackItalic:'Montserrat-BlackItalic'
    ,Montserrat_Bold:'Montserrat-Bold'
    ,Montserrat_BoldItalic:'Montserrat-BoldItalic'
    ,Montserrat_ExtraBold:'Montserrat-ExtraBold'
    ,Montserrat_ExtraBoldItalic:'Montserrat-ExtraBoldItalic'
    ,Montserrat_ExtraLight:'Montserrat-ExtraLight'
    ,Montserrat_Italic:'Montserrat-Italic'
    ,Montserrat_Thin:'Montserrat-Thin'
    ,Montserrat_Light:'Montserrat-Light'
    ,Montserrat_Medium:'Montserrat-Medium'
} 
export {FONTS}